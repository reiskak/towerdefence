// GENERATED AUTOMATICALLY FROM 'Assets/Input/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Inputs"",
            ""id"": ""c381a727-77e9-4679-9cd0-52a26c3ec17d"",
            ""actions"": [
                {
                    ""name"": ""One"",
                    ""type"": ""Button"",
                    ""id"": ""4a0192aa-2296-41a3-9c3e-c542267e232f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Two"",
                    ""type"": ""Button"",
                    ""id"": ""84f89e9c-ead8-4c5e-b723-af24194c0818"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Three"",
                    ""type"": ""Button"",
                    ""id"": ""8397ef6d-3307-4d4d-ae93-e1eb20186afe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Four"",
                    ""type"": ""Button"",
                    ""id"": ""6b489a47-0e5d-40b1-931a-d8cb8d778c73"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Five"",
                    ""type"": ""Button"",
                    ""id"": ""22916bf8-b00c-407b-94d6-d498484a50b8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Six"",
                    ""type"": ""Button"",
                    ""id"": ""16961e7f-9034-4e98-9a46-a741ec705dc8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Seven"",
                    ""type"": ""Button"",
                    ""id"": ""0c11a23f-3062-4d98-afb9-e88248fa8862"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Eight"",
                    ""type"": ""Button"",
                    ""id"": ""844cb937-367c-45c8-8729-3fb6aae7c05e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Nine"",
                    ""type"": ""Button"",
                    ""id"": ""3c4016bf-52a6-4f72-b199-543198ad840d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Zero"",
                    ""type"": ""Button"",
                    ""id"": ""5b551f8b-f56d-49e2-8d10-3d29dd409c86"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UILeftClick"",
                    ""type"": ""Button"",
                    ""id"": ""ddb4fda9-190d-4c45-a518-291786cbdc33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlaceUnit"",
                    ""type"": ""Button"",
                    ""id"": ""636b63a1-5862-4850-841d-0c12dfd56787"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CancelUnitPlacement"",
                    ""type"": ""Button"",
                    ""id"": ""dff71a88-e659-46ec-80e4-410a27cff52a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotateUnit"",
                    ""type"": ""Value"",
                    ""id"": ""e0d86b5f-9643-4664-b4c2-1068251e8134"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e8e53a41-0005-4041-a623-c816c72444c8"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""One"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f54aaa02-4e88-4eb7-88fa-3a135d1688df"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Two"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5509ff4-8a51-410c-97fb-00605674457a"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Three"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""02a73b86-7825-44dc-a432-cccd19667ce8"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Four"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e02767e2-328b-4109-8f14-f1e07bd17e5d"",
                    ""path"": ""<Keyboard>/5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Five"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e8229e5a-1167-45e4-8a73-d04d394df7e6"",
                    ""path"": ""<Keyboard>/6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Six"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0e74a618-42a2-45f9-91d6-5b457654f897"",
                    ""path"": ""<Keyboard>/7"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Seven"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84f545b7-b355-4e6d-9ec5-169a186b4ba1"",
                    ""path"": ""<Keyboard>/8"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Eight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47b9ac22-8b15-4474-a05b-6d2a0557a3b5"",
                    ""path"": ""<Keyboard>/9"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Nine"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""add2fc1b-c216-4f99-b6f7-b1231d481dc9"",
                    ""path"": ""<Keyboard>/0"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Zero"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8fb9d9d6-702a-4ea8-9452-4578df90ee65"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UILeftClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad90bec8-dc0d-4920-80ca-39b232da271f"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PlaceUnit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5b888154-ed59-4549-899d-ceaf57d5e78b"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CancelUnitPlacement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cdcb0031-d545-4b11-8ad4-22b41aff8747"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotateUnit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Inputs
        m_Inputs = asset.FindActionMap("Inputs", throwIfNotFound: true);
        m_Inputs_One = m_Inputs.FindAction("One", throwIfNotFound: true);
        m_Inputs_Two = m_Inputs.FindAction("Two", throwIfNotFound: true);
        m_Inputs_Three = m_Inputs.FindAction("Three", throwIfNotFound: true);
        m_Inputs_Four = m_Inputs.FindAction("Four", throwIfNotFound: true);
        m_Inputs_Five = m_Inputs.FindAction("Five", throwIfNotFound: true);
        m_Inputs_Six = m_Inputs.FindAction("Six", throwIfNotFound: true);
        m_Inputs_Seven = m_Inputs.FindAction("Seven", throwIfNotFound: true);
        m_Inputs_Eight = m_Inputs.FindAction("Eight", throwIfNotFound: true);
        m_Inputs_Nine = m_Inputs.FindAction("Nine", throwIfNotFound: true);
        m_Inputs_Zero = m_Inputs.FindAction("Zero", throwIfNotFound: true);
        m_Inputs_UILeftClick = m_Inputs.FindAction("UILeftClick", throwIfNotFound: true);
        m_Inputs_PlaceUnit = m_Inputs.FindAction("PlaceUnit", throwIfNotFound: true);
        m_Inputs_CancelUnitPlacement = m_Inputs.FindAction("CancelUnitPlacement", throwIfNotFound: true);
        m_Inputs_RotateUnit = m_Inputs.FindAction("RotateUnit", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Inputs
    private readonly InputActionMap m_Inputs;
    private IInputsActions m_InputsActionsCallbackInterface;
    private readonly InputAction m_Inputs_One;
    private readonly InputAction m_Inputs_Two;
    private readonly InputAction m_Inputs_Three;
    private readonly InputAction m_Inputs_Four;
    private readonly InputAction m_Inputs_Five;
    private readonly InputAction m_Inputs_Six;
    private readonly InputAction m_Inputs_Seven;
    private readonly InputAction m_Inputs_Eight;
    private readonly InputAction m_Inputs_Nine;
    private readonly InputAction m_Inputs_Zero;
    private readonly InputAction m_Inputs_UILeftClick;
    private readonly InputAction m_Inputs_PlaceUnit;
    private readonly InputAction m_Inputs_CancelUnitPlacement;
    private readonly InputAction m_Inputs_RotateUnit;
    public struct InputsActions
    {
        private @Controls m_Wrapper;
        public InputsActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @One => m_Wrapper.m_Inputs_One;
        public InputAction @Two => m_Wrapper.m_Inputs_Two;
        public InputAction @Three => m_Wrapper.m_Inputs_Three;
        public InputAction @Four => m_Wrapper.m_Inputs_Four;
        public InputAction @Five => m_Wrapper.m_Inputs_Five;
        public InputAction @Six => m_Wrapper.m_Inputs_Six;
        public InputAction @Seven => m_Wrapper.m_Inputs_Seven;
        public InputAction @Eight => m_Wrapper.m_Inputs_Eight;
        public InputAction @Nine => m_Wrapper.m_Inputs_Nine;
        public InputAction @Zero => m_Wrapper.m_Inputs_Zero;
        public InputAction @UILeftClick => m_Wrapper.m_Inputs_UILeftClick;
        public InputAction @PlaceUnit => m_Wrapper.m_Inputs_PlaceUnit;
        public InputAction @CancelUnitPlacement => m_Wrapper.m_Inputs_CancelUnitPlacement;
        public InputAction @RotateUnit => m_Wrapper.m_Inputs_RotateUnit;
        public InputActionMap Get() { return m_Wrapper.m_Inputs; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InputsActions set) { return set.Get(); }
        public void SetCallbacks(IInputsActions instance)
        {
            if (m_Wrapper.m_InputsActionsCallbackInterface != null)
            {
                @One.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnOne;
                @One.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnOne;
                @One.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnOne;
                @Two.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnTwo;
                @Two.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnTwo;
                @Two.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnTwo;
                @Three.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnThree;
                @Three.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnThree;
                @Three.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnThree;
                @Four.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnFour;
                @Four.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnFour;
                @Four.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnFour;
                @Five.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnFive;
                @Five.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnFive;
                @Five.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnFive;
                @Six.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnSix;
                @Six.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnSix;
                @Six.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnSix;
                @Seven.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnSeven;
                @Seven.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnSeven;
                @Seven.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnSeven;
                @Eight.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnEight;
                @Eight.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnEight;
                @Eight.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnEight;
                @Nine.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnNine;
                @Nine.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnNine;
                @Nine.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnNine;
                @Zero.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnZero;
                @Zero.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnZero;
                @Zero.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnZero;
                @UILeftClick.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnUILeftClick;
                @UILeftClick.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnUILeftClick;
                @UILeftClick.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnUILeftClick;
                @PlaceUnit.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnPlaceUnit;
                @PlaceUnit.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnPlaceUnit;
                @PlaceUnit.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnPlaceUnit;
                @CancelUnitPlacement.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnCancelUnitPlacement;
                @CancelUnitPlacement.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnCancelUnitPlacement;
                @CancelUnitPlacement.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnCancelUnitPlacement;
                @RotateUnit.started -= m_Wrapper.m_InputsActionsCallbackInterface.OnRotateUnit;
                @RotateUnit.performed -= m_Wrapper.m_InputsActionsCallbackInterface.OnRotateUnit;
                @RotateUnit.canceled -= m_Wrapper.m_InputsActionsCallbackInterface.OnRotateUnit;
            }
            m_Wrapper.m_InputsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @One.started += instance.OnOne;
                @One.performed += instance.OnOne;
                @One.canceled += instance.OnOne;
                @Two.started += instance.OnTwo;
                @Two.performed += instance.OnTwo;
                @Two.canceled += instance.OnTwo;
                @Three.started += instance.OnThree;
                @Three.performed += instance.OnThree;
                @Three.canceled += instance.OnThree;
                @Four.started += instance.OnFour;
                @Four.performed += instance.OnFour;
                @Four.canceled += instance.OnFour;
                @Five.started += instance.OnFive;
                @Five.performed += instance.OnFive;
                @Five.canceled += instance.OnFive;
                @Six.started += instance.OnSix;
                @Six.performed += instance.OnSix;
                @Six.canceled += instance.OnSix;
                @Seven.started += instance.OnSeven;
                @Seven.performed += instance.OnSeven;
                @Seven.canceled += instance.OnSeven;
                @Eight.started += instance.OnEight;
                @Eight.performed += instance.OnEight;
                @Eight.canceled += instance.OnEight;
                @Nine.started += instance.OnNine;
                @Nine.performed += instance.OnNine;
                @Nine.canceled += instance.OnNine;
                @Zero.started += instance.OnZero;
                @Zero.performed += instance.OnZero;
                @Zero.canceled += instance.OnZero;
                @UILeftClick.started += instance.OnUILeftClick;
                @UILeftClick.performed += instance.OnUILeftClick;
                @UILeftClick.canceled += instance.OnUILeftClick;
                @PlaceUnit.started += instance.OnPlaceUnit;
                @PlaceUnit.performed += instance.OnPlaceUnit;
                @PlaceUnit.canceled += instance.OnPlaceUnit;
                @CancelUnitPlacement.started += instance.OnCancelUnitPlacement;
                @CancelUnitPlacement.performed += instance.OnCancelUnitPlacement;
                @CancelUnitPlacement.canceled += instance.OnCancelUnitPlacement;
                @RotateUnit.started += instance.OnRotateUnit;
                @RotateUnit.performed += instance.OnRotateUnit;
                @RotateUnit.canceled += instance.OnRotateUnit;
            }
        }
    }
    public InputsActions @Inputs => new InputsActions(this);
    public interface IInputsActions
    {
        void OnOne(InputAction.CallbackContext context);
        void OnTwo(InputAction.CallbackContext context);
        void OnThree(InputAction.CallbackContext context);
        void OnFour(InputAction.CallbackContext context);
        void OnFive(InputAction.CallbackContext context);
        void OnSix(InputAction.CallbackContext context);
        void OnSeven(InputAction.CallbackContext context);
        void OnEight(InputAction.CallbackContext context);
        void OnNine(InputAction.CallbackContext context);
        void OnZero(InputAction.CallbackContext context);
        void OnUILeftClick(InputAction.CallbackContext context);
        void OnPlaceUnit(InputAction.CallbackContext context);
        void OnCancelUnitPlacement(InputAction.CallbackContext context);
        void OnRotateUnit(InputAction.CallbackContext context);
    }
}
