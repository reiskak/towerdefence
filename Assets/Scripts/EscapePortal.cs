﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapePortal : MonoBehaviour
{
    public delegate void EnemyEscaped();
    public static event EnemyEscaped EnemyEscapedEvent;
    
    
    private void OnTriggerEnter(Collider enemy)
    {
        if (!enemy.name.StartsWith("Enemy"))
            return;
        Destroy(enemy.gameObject);
        EnemyEscapedEvent();
    }
}
