﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class GameController : MonoBehaviour
{
    // Terrain generation related variables
    public Maze mazePrefab;
    private Maze mazeInstance;
    public TerrainManager terrainManager;
    public Vector2Int MapSize;

    
    // Gold related variables
    public int startingGold;

    
    // Wave related variables
    public TMP_Text timeUI;
    public TMP_Text waveClearUI;
    private int timeTillNextWave;
    public TMP_Text waveText;
    private EnemyController enemyController;
    public int wave = 1;
    public delegate void WaveClear(int wave);
    public static event WaveClear WaveClearEvent;
    
    
    public int TimeTillNextWave
    {
        get
        {
            return timeTillNextWave;
        }
        set
        {
            timeUI.text = value.ToString();
            timeTillNextWave = value;
        }
    }
    
    // Losing game related variables
    public TMP_Text escapedUI;
    public TMP_Text gameLostUI;
    private int enemiesEscaped;
    public int EnemiesEscaped
    {
        get
        {
            return enemiesEscaped;
        }
        set
        {
            escapedUI.text = value.ToString() + "/" + maxEscapedEnemies.ToString();
            enemiesEscaped = value;
        }
    }
    public int maxEscapedEnemies;

    // Control confirm related variables
    public GameObject ControlsUI;
    private bool started = false;
    private PlayerInput inputManager;

    private bool lost = false;
    
    
    void Start()
    {
     //   GenerateTerrain();
     //   return;
        enemyController = GameObject.FindObjectOfType<EnemyController>();
        GoldManager.instance.Gold = startingGold;
        TimeTillNextWave = 30;
        inputManager = GameObject.Find("InputManager").GetComponent<PlayerInput>();
        inputManager.onActionTriggered += InputManagerOnonActionTriggered;
        //Listen to enemy escape events
        EscapePortal.EnemyEscapedEvent += EscapePortalOnEnemyEscapedEvent;
        EnemiesEscaped = 0;
    }

    private void InputManagerOnonActionTriggered(InputAction.CallbackContext context)
    {
        if (context.action.name == "Confirm")
        {
            if (started)
            {
                if(lost)
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                return;
            }
            else
            {
                ControlsUI.SetActive(false);
                StartCoroutine(WaveManager());
                started = true;
            }
        }
    }


    
    private void EscapePortalOnEnemyEscapedEvent()
    {
        EnemiesEscaped++;
        if (EnemiesEscaped >= maxEscapedEnemies)
            EndGame(false);
    }

    private void EndGame(bool win)
    {
        if(win)
            Debug.Log("YOU WON");
        else
        {
            lost = true;
            enemyController.Spawning = false;
            StopCoroutine(WaveManager());
            // Show game lost UI
            gameLostUI.text = $"YOU LOST \nSurvived {wave} waves \nPress Space to restart";
            gameLostUI.gameObject.SetActive(true);
        }

    }
    // Handle game timings
    IEnumerator WaveManager()
    {
        while (true)
        {
            // Count down to 0
            while (TimeTillNextWave > 0)
            {
                TimeTillNextWave--;
                yield return new WaitForSeconds(1);
            }
            // If we were on spawning phase, update ui.
            if (enemyController.Spawning)
            {
                waveClearUI.text = $"Wave {wave} Clear!";
                waveClearUI.gameObject.SetActive(true);
                wave++;
                WaveClearEvent(wave);
                waveText.text = "Next Wave";
                TimeTillNextWave = 20;
            }
            // If we were on "build" phase,  update ui.
            else
            {
                waveText.text = "Wave left";
                TimeTillNextWave = 30;
            }
            // Toggle spawning
            enemyController.ToggleSpawn();

        }
    }
    

    

    private void GenerateTerrain()
    {
        terrainManager.SetTerrainSize(MapSize);
        mazePrefab.size = MapSize;
        GenerateMaze();
        var OpenTiles = new List<MazeCell>();
        GetPathOnMaze(ref OpenTiles,new Vector2Int(0,0));
        GetPathOnMaze(ref OpenTiles,new Vector2Int(MapSize.x-1,MapSize.y-1));
        byte[,] trees = new byte[MapSize.x, MapSize.y];
        foreach (var mazeCell in OpenTiles)
        {
            trees[mazeCell.Coordinates.x, mazeCell.Coordinates.y] = 1;
        }

        MathAlgorithms.getCenterOfBiggestSquare(trees, out Vector2 coordinates, out int squaresize);
        if(squaresize > 2)
            terrainManager.SpawnLakeAt(coordinates,squaresize);
        var sw = Stopwatch.StartNew();
        for (int i = 0; i < MapSize.x; i++)
        {
            for (int j = 0; j < MapSize.y; j++)
            {
                if(trees[i,j] == 1)
                    terrainManager.FillTileWithTrees(new Vector2(i,j));
                else
                    terrainManager.SpawnRandomObjects(new Vector2(i,j));
            }
        }
        sw.Stop();
        Debug.Log("Tree generation took:"+ sw.ElapsedMilliseconds + "ms");
        terrainManager.CleanUpUnderwaterObjects();
        mazeInstance.gameObject.SetActive(false);
    }


    private void GetPathOnMaze(ref List<MazeCell> OpenTiles,Vector2Int start)
    {
        var algorithms = new Algorithms();
        var shortestPath = algorithms.ShortestPathFunction(mazeInstance.Graph, start);
        var goal = new Vector2Int(MapSize.x/2,MapSize.y/2);
        bool pathfound = false;
        IEnumerable<Vector2Int> path = null;
        int failSafe = 0;
        while (!pathfound)
        {
            try
            {
                path = shortestPath(goal);
                pathfound = true;
            }
            catch
            {
                Debug.Log("failed to find path to:" + goal);
                goal += new Vector2Int(UnityEngine.Random.Range(-1,2), UnityEngine.Random.Range(-1, 2));
                failSafe++;
                if (failSafe > 50)
                {
                    break;
                }
            }
        }
        /*
        HashSet<MazeRoom> roomsOnRoute = new HashSet<MazeRoom>();
        foreach (var vector2Int in path)
        {
            var cell = mazeInstance.GetCell(vector2Int);
            if (!roomsOnRoute.Contains(cell.room)) roomsOnRoute.Add(cell.room);
            cell.GetComponent<Renderer>().material.color = Color.red;
        }
        List<MazeCell> cellsOnRooms = new List<MazeCell>();
        foreach (var mazeRoom in roomsOnRoute)
        {
            cellsOnRooms.AddRange(mazeRoom.cells);
        }*/
        //List<MazeCell> cellsOnRooms = new List<MazeCell>();
        foreach (var vector2Int in path)
        {
            OpenTiles.Add(mazeInstance.GetCell(vector2Int));
        }
        OpenTiles.AddRange(mazeInstance.GetCell(path.First()).room.cells);
        OpenTiles.AddRange(mazeInstance.GetCell(path.Last()).room.cells);
    }
    
    private void GenerateMaze()
    {
        
        mazeInstance = Instantiate(mazePrefab) as Maze;
        var sw = Stopwatch.StartNew();
        mazeInstance.GenerateNoDelay();
        Debug.Log(sw.ElapsedMilliseconds);
        sw.Stop();
        
        // Debug.Log($"shortest path to {goal}: {string.Join(", ", shortestPath(goal))}");
        //StartCoroutine(mazeInstance.Generate());
    }
}
