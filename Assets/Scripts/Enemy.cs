﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public delegate void Death(Enemy enemy);
    public static event Death DeathEvent;

    public int goldValue;
    public int health;
    public int projectilesComing = 0;
    
    // Play animations when hit.
    public void HitEnemy()
    {
        projectilesComing--;
        GetComponent<Animator>().SetTrigger("Damaged");
        if (health <= 0 && projectilesComing == 0)
        {
            GetComponent<NavMeshAgent>().isStopped = true;
            GetComponent<Animator>().SetBool("Dead",true);
        }
    }
    // Send death event, called after Dead animation finished.
    public void EventDead()
    {
        DeathEvent(this);
    }
}
