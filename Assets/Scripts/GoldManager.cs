﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldManager : MonoBehaviour
{
    public static GoldManager instance { get; private set; }
    
    public TMP_Text goldUI;
    private int gold;
    public int Gold
    {
        get
        {
            return gold;
        }
        set
        {
            goldUI.text = value.ToString();
            gold = value;
        }
    }
    void Awake()
    {
        instance = this;
    }
}
