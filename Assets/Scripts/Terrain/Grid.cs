﻿using UnityEditor;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [SerializeField]
    private float size = 1f;
    [SerializeField]
    private bool showLabels ;
    public Vector3 GetNearestPointOnGrid(Vector3 position)
    {
        position -= transform.position;

        int xCount = Mathf.RoundToInt(position.x / size);
        int yCount = Mathf.RoundToInt(position.y / size);
        int zCount = Mathf.RoundToInt(position.z / size);

        Vector3 result = new Vector3(
            (float)xCount * size,
            (float)yCount * size,
            (float)zCount * size);

        result += transform.position;

        return result;
    }
    
    // Draws grid and labers with gizmos in editor
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        
        for (float x = 0; x < 200; x += size)
        {
            for (float z = 0; z < 200; z += size)
            {
                var point = GetNearestPointOnGrid(new Vector3(x, transform.position.y, z));
                if(showLabels)
                    Handles.Label(point+new Vector3(5,0,5), $"{x/size} {z/size}");
                Gizmos.DrawSphere(point, 0.5f);
            }
                
        }
    }
#endif
}