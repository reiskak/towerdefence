﻿using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class GenerateTrees : MonoBehaviour
{
    [FormerlySerializedAs("Terrain")] public Terrain terrain;

    public Vector2Int terrainSizeInCoordinates;
    public int coordinatesToTerrainScale = 10;

    // Minium distance between spaned trees
    public float TreeMinDistance;
    // Start is called before the first frame update
    void Start()
    {
        terrain.terrainData.treeInstances =  new List<TreeInstance>().ToArray();
        terrain.detailObjectDistance = 1000000f;
        SetTerrainSize(new Vector2Int(20,20));
        FillTileWithTrees(new Vector2(0,0));
        FillTileWithTrees(new Vector2(1,0));
        FillTileWithTrees(new Vector2(2,0));
        FillTileWithTrees(new Vector2(1,1));
        FillTileWithTrees(new Vector2(1,2));
    }

    public void SetTerrainSize(Vector2Int Size)
    {
        terrainSizeInCoordinates = Size;
        terrain.terrainData.size = new Vector3(Size.x * coordinatesToTerrainScale, 100, Size.y * coordinatesToTerrainScale);
    }

    public void FillTileWithTrees(Vector2 Coordinates)
    {
        var scaled = Coordinates * coordinatesToTerrainScale;
        SpawnTrees(new Vector3( scaled.x + (float)coordinatesToTerrainScale/2,0,scaled.y + (float)coordinatesToTerrainScale/2),coordinatesToTerrainScale-1,20);
    }

    private void SpawnTrees(Vector3 TileCenter, int TileSize,int Treecount)
    {
        List<Vector3> preNormalisedPositions = new List<Vector3>();
        var treeDisplacement = Vector3.zero;
        for (int i = 0; i < Treecount; i++)
        {
            var newTree = new TreeInstance();
            newTree.prototypeIndex = 0;
            newTree.color = Color.white;
            newTree.lightmapColor = Color.white;
            newTree.heightScale = Random.Range(0.8f,1.2f);
            newTree.widthScale = 1;
            var tileRadius = (float)TileSize / (float)2;
            var terrainDateSize = terrain.terrainData.size;
            bool maxTriesReached = false;
            int tries = 0;
            while (true)
            {
                treeDisplacement = new Vector3(Random.Range(-tileRadius, tileRadius),0,Random.Range(-tileRadius, tileRadius)); // World Space Coords Random around tilecenter
                if (IsGoodPosition(treeDisplacement,preNormalisedPositions))
                    break;
                tries++;
                if (tries > 20)
                {
                    Debug.Log("Failed to find space for tree");
                    maxTriesReached = true;
                    break;
                }
            }
            if(maxTriesReached)
                continue;
            preNormalisedPositions.Add(treeDisplacement);

            newTree.position = new Vector3(TileCenter.x + treeDisplacement.x,TileCenter.y + treeDisplacement.y,TileCenter.z + treeDisplacement.z );
            var newtreeterrainLocalPos = newTree.position - terrain.transform.position;
            var newtreenormalizedPos = new Vector2(Mathf.InverseLerp(0.0f, terrainDateSize.x, newtreeterrainLocalPos.x), Mathf.InverseLerp(0.0f, terrainDateSize.z, newtreeterrainLocalPos.z));
            var pos = new Vector3(newtreenormalizedPos.x, 0, newtreenormalizedPos.y);
            newTree.position =  pos;
            this.terrain.AddTreeInstance(newTree);
        }
    }

    private bool IsGoodPosition(Vector3 pos, List<Vector3> existingTrees)
    {
        foreach (var position in existingTrees)
        {
            var dist = Vector3.Distance(position, pos);
//            Debug.Log( position + " " + pos + " " + dist.ToString());
            if (dist < TreeMinDistance)
            {
                return false;
            }
        }
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
