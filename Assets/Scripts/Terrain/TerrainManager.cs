﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class TerrainManager : MonoBehaviour
{
    
    private Terrain terrain;    
    
    public Vector2Int terrainSizeInCoordinates;
    public int coordinatesToTerrainScale = 10;
    int mapsize = 200;
    int hmapsize = 512;
    
    [FormerlySerializedAs("TreeMinDistance")] public float treeMinDistance;
    
    [FormerlySerializedAs("River Stamp")] public Texture2D riverStamp;
    
    [FormerlySerializedAs("Lake Stamp")] public Texture2D lakeStamp;
    
    // Start is called before the first frame update
    void Start()
    {
        terrain = Terrain.activeTerrain;
        terrain.treeBillboardDistance = 0;
        //  FlattenTerrain();
        // terrain.terrainData.treeInstances =  new List<TreeInstance>().ToArray();
    }
    
    public void SetTerrainSize(Vector2Int size)
    {
        terrainSizeInCoordinates = size;
        terrain.terrainData.size = new Vector3(size.x * coordinatesToTerrainScale, 100, size.y * coordinatesToTerrainScale);
    }

    public void FillTileWithTrees(Vector2 coordinates)
    {
        var scaled = coordinates * coordinatesToTerrainScale;
        SpawnObjects(new Vector3( scaled.x + (float)coordinatesToTerrainScale/2,0,scaled.y + (float)coordinatesToTerrainScale/2),coordinatesToTerrainScale-1,20);
    }
    
    public void SpawnRandomObjects(Vector2 coordinates)
    {
        var scaled = coordinates * coordinatesToTerrainScale;
        SpawnObjects(new Vector3( scaled.x + (float)coordinatesToTerrainScale/2,0,scaled.y + (float)coordinatesToTerrainScale/2),coordinatesToTerrainScale-1,Random.Range(0,3),1);
        SpawnObjects(new Vector3( scaled.x + (float)coordinatesToTerrainScale/2,0,scaled.y + (float)coordinatesToTerrainScale/2),coordinatesToTerrainScale-1,Random.Range(0,2),0);
    }
    
    public void FlattenTerrain()
    {
        var terrainData = terrain.terrainData;
        var heights = terrainData.GetHeights(0, 0, terrainData.heightmapResolution, terrainData.heightmapResolution);
 
        for (int x = 0; x < terrainData.heightmapResolution; x++) {
            for (int y = 0; y < terrainData.heightmapResolution; y++) {
                heights[x,y] = 0.4f;
            }
        }
        terrainData.SetHeights(0, 0, heights);
    }

    private void SpawnObjects(Vector3 tileCenter, int tileSize,int objectCount, int objectIdx = 0)
    {
        List<Vector3> preNormalisedPositions = new List<Vector3>();
        Vector3 treeDisplacement;
        for (int i = 0; i < objectCount; i++)
        {
            var newTree = new TreeInstance();
            newTree.prototypeIndex = objectIdx;
            newTree.color = Color.white;
            newTree.lightmapColor = Color.white;
            newTree.heightScale = Random.Range(0.8f,1.2f);
            newTree.widthScale = 1;
            var tileRadius = (float)tileSize / (float)2;
            var terrainDateSize = terrain.terrainData.size;
            bool maxTriesReached = false;
            int tries = 0;
            while (true)
            {
                treeDisplacement = new Vector3(Random.Range(-tileRadius, tileRadius),0,Random.Range(-tileRadius, tileRadius)); // World Space Coords Random around tilecenter
                if (IsGoodPosition(treeDisplacement,preNormalisedPositions))
                    break;
                tries++;
                if (tries > 20)
                {
                 //   Debug.Log("Failed to find space for tree");
                    maxTriesReached = true;
                    break;
                }
            }
            if(maxTriesReached)
                continue;
            preNormalisedPositions.Add(treeDisplacement);

            newTree.position = new Vector3(tileCenter.x + treeDisplacement.x,tileCenter.y + treeDisplacement.y,tileCenter.z + treeDisplacement.z );
            var newtreeterrainLocalPos = newTree.position - terrain.transform.position;
            var newtreenormalizedPos = new Vector2(Mathf.InverseLerp(0.0f, terrainDateSize.x, newtreeterrainLocalPos.x), Mathf.InverseLerp(0.0f, terrainDateSize.z, newtreeterrainLocalPos.z));
            var pos = new Vector3(newtreenormalizedPos.x, 0, newtreenormalizedPos.y);
            newTree.position =  pos;
            this.terrain.AddTreeInstance(newTree);
        }
    }

    private bool IsGoodPosition(Vector3 pos, List<Vector3> existingTrees)
    {
        foreach (var position in existingTrees)
        {
            var dist = Vector3.Distance(position, pos);
//            Debug.Log( position + " " + pos + " " + dist.ToString());
            if (dist < treeMinDistance)
            {
                return false;
            }
        }
        return true;
    }
    
    private Texture2D rotateStamp(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;
     
        int iRotated, iOriginal;
     
        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }
     
        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }
    //https://answers.unity.com/questions/1349349/heightmap-from-texture-script-converter.html
    private void StampTerrain(Vector2Int position, Texture2D texture, float strenght, int size)
    {
        int w = texture.width;
        int h = texture.height;
        int w2 = size;
        var terrainData = terrain.terrainData;
        float[,] heightmapData = terrainData.GetHeights(position.x, position.y, terrainData.heightmapResolution-position.x, terrainData.heightmapResolution-position.y);
        Color[] mapColors = texture.GetPixels();
        Color[] map = new Color[w2 * w2];
        if (w2 != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (texture.filterMode == FilterMode.Point)
            {
                float dx = (float) w / (float) w2;
                float dy = (float) h / (float) w2;
                for (int y = 0; y < w2; y++)
                {

                    int thisY = Mathf.FloorToInt(dy * y) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        map[yw + x] = mapColors[Mathf.FloorToInt(thisY + dx * x)];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else
            {
                float ratioX = (1.0f / ((float) w2 / (w - 1)));
                float ratioY = (1.0f / ((float) w2 / (h - 1)));
                for (int y = 0; y < w2; y++)
                {

                    int yy = Mathf.FloorToInt(y * ratioY);
                    int y1 = yy * w;
                    int y2 = (yy + 1) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        int xx = Mathf.FloorToInt(x * ratioX);
                        Color bl = mapColors[y1 + xx];
                        Color br = mapColors[y1 + xx + 1];
                        Color tl = mapColors[y2 + xx];
                        Color tr = mapColors[y2 + xx + 1];
                        float xLerp = x * ratioX - xx;
                        map[yw + x] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp),
                            y * ratioY - (float) yy);
                    }
                }
            }
            
        }
        else
        {
            // Use original if no resize is needed
            map = mapColors;
        }

        // Assign texture data to heightmap
        for (int y = 0; y < w2; y++)
        {
            for (int x = 0; x < w2; x++)
            {
                heightmapData[y, x] = heightmapData[y, x] - strenght * map[y * w2 + x].grayscale;
            }
        }

        terrainData.SetHeights(position.x, position.y, heightmapData);
    }

    public void CleanUpUnderwaterObjects()
    {
        List<int> objectIdxToRemove = new List<int>();
        var objects = terrain.terrainData.treeInstances;
        var currentObjects = terrain.terrainData.treeInstances.ToList();
        var terrainSize = terrain.terrainData.size;
        var terrainPos = terrain.transform.position;
        for (int i = 0; i < objects.Length; i++)
        {
            var current = objects[i];
            Vector3 worldPosition = Vector3.Scale(current.position, terrainSize) +terrainPos ;
            
            if (worldPosition.y < 39.8f)
            {
                Debug.Log(worldPosition);
                currentObjects.Remove(current);
            }
        }

        terrain.terrainData.treeInstances = currentObjects.ToArray();
    }

    public void SpawnLakeAt(Vector2 lakePosition, int size)
    {
        lakePosition += Vector2.one;
        var stampsize = (int)Math.Round((float) hmapsize / mapsize * 10*(size));
        Debug.Log(stampsize);
        Vector2Int position = new Vector2Int((int)Math.Round((float)hmapsize / mapsize*(lakePosition.x * 10)), (int)Math.Round((float)hmapsize / mapsize*(lakePosition.y * 10)));
        StampTerrain(position,lakeStamp,0.3f,stampsize);
    }
    
    public void SpawnRiverBetween(Vector2Int first,Vector2Int second)
    {


        var stamp = riverStamp;
        Vector2 middleCoordinate = (new Vector2(first.x,first.y) + new Vector2(second.x,second.y)) / 2;
        if(middleCoordinate.y == Math.Floor(middleCoordinate.y))
            stamp = rotateStamp(riverStamp,true);
        Vector2Int position = new Vector2Int((int)Math.Round((float)hmapsize / mapsize*(middleCoordinate.x * 10)), (int)Math.Round((float)hmapsize / mapsize*(middleCoordinate.y * 10)));
        //string heightmapPath = EditorUtility.OpenFilePanel("Texture", GetFolderPath(SpecialFolder.Desktop), ".png");
        var size = (int)Math.Round((float) hmapsize / mapsize * 11);
        StampTerrain(position,stamp,0.1f,size);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
