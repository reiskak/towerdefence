﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using UnityEditor.Experimental.TerrainAPI;
using UnityEngine;

public class HeightMapTests : MonoBehaviour
{
    // Start is called before the first frame update
    public Texture2D Stamp;
    private TerrainData terrain;
    
    void Start()
    {
        terrain = Terrain.activeTerrain.terrainData;
        FlattenTerrain();
        RiverBetweenCoordinates(new Vector2Int(5,5),new Vector2Int(5,6));
        RiverBetweenCoordinates(new Vector2Int(5,6),new Vector2Int(6,6));
    }


    //https://answers.unity.com/questions/951835/rotate-the-contents-of-a-texture.html
    private Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;
     
        int iRotated, iOriginal;
     
        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }
     
        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    public void FlattenTerrain()
    {
        var terrainX = terrain.heightmapResolution;
        var terrainY = terrain.heightmapResolution;
        var heights = terrain.GetHeights(0, 0, terrainX, terrainY);
 
        for (int x = 0; x < terrainX; x++) {
            for (int y = 0; y < terrainY; y++) {
                heights[x,y] = 0;
            }
        }
        terrain.SetHeights(0, 0, heights);
    }
    
    //https://answers.unity.com/questions/1349349/heightmap-from-texture-script-converter.html
    void RiverBetweenCoordinates(Vector2Int first,Vector2Int second)
    {
        int mapsize = 200;
        int hmapsize = 512;
        Texture2D heightmap = Stamp; //rotateTexture(Stamp,true);
        Vector2 middleCoordinate = (new Vector2(first.x,first.y) + new Vector2(second.x,second.y)) / 2;
        if(middleCoordinate.y == Math.Floor(middleCoordinate.y))
            heightmap = rotateTexture(Stamp,true);
        Vector2Int position = new Vector2Int((int)Math.Round((float)hmapsize / mapsize*(middleCoordinate.x * 10)), (int)Math.Round((float)hmapsize / mapsize*(middleCoordinate.y * 10)));
        //string heightmapPath = EditorUtility.OpenFilePanel("Texture", GetFolderPath(SpecialFolder.Desktop), ".png");
        var size = (int)Math.Round((float) hmapsize / mapsize * 11);
        
        if (heightmap == null)
        {
            Debug.Log("Failed to load heightmap");
            return;
        }
        int w = heightmap.width;
        int h = heightmap.height;
        int w2 = size;
        float[,] heightmapData = terrain.GetHeights(position.x, position.y, terrain.heightmapResolution-position.x, terrain.heightmapResolution-position.y);
        Color[] mapColors = heightmap.GetPixels();
        Color[] map = new Color[w2 * w2];
        if (w2 != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (heightmap.filterMode == FilterMode.Point)
            {
                float dx = (float) w / (float) w2;
                float dy = (float) h / (float) w2;
                for (int y = 0; y < w2; y++)
                {

                    int thisY = Mathf.FloorToInt(dy * y) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        map[yw + x] = mapColors[Mathf.FloorToInt(thisY + dx * x)];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else
            {
                float ratioX = (1.0f / ((float) w2 / (w - 1)));
                float ratioY = (1.0f / ((float) w2 / (h - 1)));
                for (int y = 0; y < w2; y++)
                {

                    int yy = Mathf.FloorToInt(y * ratioY);
                    int y1 = yy * w;
                    int y2 = (yy + 1) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        int xx = Mathf.FloorToInt(x * ratioX);
                        Color bl = mapColors[y1 + xx];
                        Color br = mapColors[y1 + xx + 1];
                        Color tl = mapColors[y2 + xx];
                        Color tr = mapColors[y2 + xx + 1];
                        float xLerp = x * ratioX - xx;
                        map[yw + x] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp),
                            y * ratioY - (float) yy);
                    }
                }
            }
            
        }
        else
        {
            // Use original if no resize is needed
            map = mapColors;
        }

        // Assign texture data to heightmap
        for (int y = 0; y < w2; y++)
        {
            for (int x = 0; x < w2; x++)
            {
                heightmapData[y, x] = heightmapData[y, x] - 0.1f * map[y * w2 + x].grayscale;
            }
        }

        terrain.SetHeights(position.x, position.y, heightmapData);
    }


    // Update is called once per frame
    void Update()
    {
    }
    
    
    /*
    // https://answers.unity.com/questions/1084016/how-to-use-a-script-to-import-terrain-raw.html
    void LoadTerrain(string aFileName, TerrainData aTerrain)
    {
        Debug.Log(aTerrain.heightmapResolution);
        var heightmap = aTerrain.GetHeights(0,0,aTerrain.heightmapResolution-1,aTerrain.heightmapResolution-1);
        
        int h = 512;
        int w = 512;
        float[,] data = new float[h, w];
        using (var file = System.IO.File.OpenRead(aFileName))
        using (var reader = new System.IO.BinaryReader(file))
        {
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    float v = (float) reader.ReadUInt16() / 0xFFFF;
                    heightmap[x, y] = heightmap[x, y] - 0.5f * v;
                    data[y, x] = v;
                }
            }
        }

        Debug.Log("Success");
        aTerrain.SetHeights(0, 0, heightmap);
    }*/
}