﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    class CameraState
    {
        public float yaw;
        public float pitch;
        public float roll;
        public float x;
        public float y;
        public float z;

        public void SetFromTransform(Transform t)
        {
            pitch = t.eulerAngles.x;
            yaw = t.eulerAngles.y;
            roll = t.eulerAngles.z;
            x = t.position.x;
            y = t.position.y;
            z = t.position.z;
        }

        public void Translate(Vector3 translation)
        {
            Vector3 rotatedTranslation = Quaternion.Euler(pitch, yaw, roll) * translation;
            var newx = x + rotatedTranslation.x;
            if(newx > 0 && newx <200)
                x = newx;
            var newz = z + rotatedTranslation.z;
            if(newz> 0 && newz <200)
                z = newz;
        }

        public void UpdateTransform(Transform t)
        {
            t.eulerAngles = new Vector3(pitch, yaw, roll);
            t.position = new Vector3(x, y, z);
        }
    }
    
    private PlayerInput inputManager;
    private bool up, down, left, right,rotateleft,rotateright;
    CameraState m_TargetCameraState = new CameraState();
    
    // Start is called before the first frame update
    void Start()
    {
        inputManager = GameObject.Find("InputManager").GetComponent<PlayerInput>();
        inputManager.onActionTriggered += InputManagerOnonActionTriggered;
        m_TargetCameraState.SetFromTransform(transform);
    }
    
    private void InputManagerOnonActionTriggered(InputAction.CallbackContext context)
    {
        switch (context.action.name)
        {
            case "MoveUp":
                up = true;
                if (context.canceled)
                    up = false;
                break;
            case "MoveDown":
                down = true;
                if (context.canceled)
                    down = false;
                break;
            case "MoveLeft":
                left = true;
                if (context.canceled)
                    left = false;
                break;
            case "MoveRight":
                right = true;
                if (context.canceled)
                    right = false;
                break;
            case "RotateRight":
                rotateright = true;
                if (context.canceled)
                    rotateright = false;
                break;
            case "RotateLeft":
                rotateleft = true;
                if (context.canceled)
                    rotateleft = false;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 translation = Vector3.zero;
        Vector3 direction = new Vector3();
        if(up)
            direction += Vector3.forward;
        if(down)
            direction += Vector3.back;
        if(left)
            direction += Vector3.left;
        if(right)
            direction += Vector3.right;
        translation = direction * Time.deltaTime;
        translation *= 50.0f;
        if(rotateleft)
            m_TargetCameraState.yaw += -1f * 80f * Time.deltaTime;
        if(rotateright)
            m_TargetCameraState.yaw += 1f * 80f * Time.deltaTime;
        m_TargetCameraState.Translate(translation);
        m_TargetCameraState.UpdateTransform(transform);

    }
}
