﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public Camera layerCamera;
    public Camera mapCamera;
    private WaitForSeconds fps = new WaitForSeconds(1 / 30f);
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TakeMinimapTexture());
        StartCoroutine(LayerMinimapRoutine());
    }
    
    
    // Take minimap screenshot for map.
    private IEnumerator TakeMinimapTexture()
    {
        yield return new WaitForEndOfFrame();
        mapCamera.Render();
    }
    // Take minimap screenshot for minimap layer with specified fps. 
    IEnumerator LayerMinimapRoutine()
    {
        while (true)
        {
            layerCamera.Render();
            yield return fps;
        }
    }

}
