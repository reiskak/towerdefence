﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    
 
    public float speed = 10;
    
    public Transform enemy;
    private float enemyDistance;
    public float delete_distance = 0.5f;
    private bool move_flag = true;
    
    public void Shoot()
    {
        enemyDistance = Vector3.Distance(transform.position, enemy.position);
        StartCoroutine(ArrowMovement());
    }
    
    
    // Function that makes arrow fly in parabolic curve, but still tracks moving enemy
    IEnumerator ArrowMovement()
    {
        while (move_flag)
        {
            if(enemy == null)
                CleanUp();
            Vector3 targetPos = enemy.position;
            // towards the target to calculate the movement
            transform.LookAt(targetPos);
            // Attenuate angle according to distance
            float angle = Mathf.Min(1, Vector3.Distance(transform.position, targetPos) / enemyDistance) * 45;
            // Rotate the corresponding angle (linearly interpolate a certain angle, and then rotate around the X axis in each frame)
            transform.rotation = transform.rotation * Quaternion.Euler(Mathf.Clamp(-angle, -42, 42), 0, 0);
            // current distance to the target point
            float currentDist = Vector3.Distance(transform.position, enemy.position);
            // Very close to the goal, ready to end the loop
            if (currentDist < delete_distance)
            {
                move_flag = false; 
            }
            // Translation (moving towards the Z axis)
            transform.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime, currentDist));
            // Pause execution, wait for the next frame to execute while
            yield return null;
        }
        if (move_flag == false)
        {
            // Make your position coincide with [target point]
            transform.position = enemy.position;
            enemy.gameObject.GetComponent<Enemy>().HitEnemy();
            CleanUp();
        }
    }

    private void CleanUp()
    {
        // [Stop] The current coroutine task, the parameter is the name of the coroutine method
        StopCoroutine(ArrowMovement());
        // Destroy the script
        GameObject.Destroy(this.gameObject);
    }

}
