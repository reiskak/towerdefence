﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRenderFix : MonoBehaviour
{
    //Fix particles rendering over billboard trees
    void Start()
    {
        GetComponent<Renderer>().material.renderQueue=2800;
    }

}
