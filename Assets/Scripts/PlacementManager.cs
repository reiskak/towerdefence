﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PlacementManager : MonoBehaviour
{
    [SerializeField] private List<PlaceableUnit> unitPrefabs;
    public Material placementMaterial;
    // sound that plays on successfully upgrading / buying unit
    public AudioClip buySound;
    // sound that plays when not enough money for action
    public AudioClip noGoldSound;
    // unit rotation speed modifier
    public float rotateSpeed = 5f;
    public TMP_Text PlacingUIText;
    public TMP_Text UpgradingUIText;
    
    private List<PlaceableUnit> placedUnits;


    private bool placing = false;
    private bool upgrading = false;
    private float mouseWheelRotation;

    private GameObject placePreviewObject;
    private bool objectPlaceable;
    private Camera cam;
    private AudioSource audioSource;
    private PlayerInput inputManager;
    
    // Current unit on mouse when upgrading, also handles color changes with layer
    private PlaceableUnit upgradeUnitOnMouse;
    public PlaceableUnit UpgradeUnitOnMouse
    {
        get
        {
            return upgradeUnitOnMouse;
        }
        set
        {
            if (upgradeUnitOnMouse != null)
                upgradeUnitOnMouse.gameObject.transform.SetLayer(0);
            upgradeUnitOnMouse = value;
            if(upgradeUnitOnMouse != null)
                upgradeUnitOnMouse.gameObject.transform.SetLayer(9);
            
        }
    }
    
    
    private GoldManager goldManager;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        placedUnits = new List<PlaceableUnit>();
        cam = Camera.main;
        goldManager = GoldManager.instance;
        inputManager = GameObject.Find("InputManager").GetComponent<PlayerInput>();
        inputManager.onActionTriggered += InputManagerOnActionTriggered;
        PopulateUnitPlacementUI();
    }
    
    // Generate UnitPlacementPanel buttons based on prefab data
    private void PopulateUnitPlacementUI()
    {
        Resources.FindObjectsOfTypeAll<PopulateUnitPanel>()[0].GenerateButtons(unitPrefabs,PlacingStart);
    }
    
    // Handle placement and upgrading input
    private void InputManagerOnActionTriggered(InputAction.CallbackContext context)
    {
        if(!context.performed)
            return;
        if (placing)
        {
            switch (context.action.name)
            {
                case "PlaceUnit":
                    if (!objectPlaceable)
                        break;
                    placePreviewObject.transform.SetLayer(0);
                    var unit = placePreviewObject.GetComponent<PlaceableUnit>();
                    if (goldManager.Gold > unit.Price)
                    {
                        goldManager.Gold -= unit.Price;
                        placedUnits.Add(unit);
                        audioSource.PlayOneShot(buySound);
                        PlacingUIText.gameObject.SetActive(false);
                        placePreviewObject.GetComponent<CapsuleCollider>().enabled = true;
                        placePreviewObject = null;
                        placing = false;
                    }
                    else
                    {
                        audioSource.PlayOneShot(noGoldSound);
                        Debug.Log("no gold");
                    }
                    break;
                case "CancelUnitPlacement":
                    CancelPlacement();
                    break;
                case "RotateUnit":
                    
                    var value = context.ReadValue<float>() * Time.deltaTime * rotateSpeed;
                    mouseWheelRotation += value;
                    break;
            }
        }
        if (upgrading)
        {
            switch (context.action.name)
            {
                case "PlaceUnit":
                    if (UpgradeUnitOnMouse != null && UpgradeUnitOnMouse.upgradeLevel < 3 &&goldManager.Gold > UpgradeUnitOnMouse.UpgradePrice)
                    {
                        goldManager.Gold -= UpgradeUnitOnMouse.UpgradePrice;
                        audioSource.PlayOneShot(buySound);
                        UpgradeUnitOnMouse.Upgrade();
                        UpgradeUnitOnMouse = null;
                    }
                    else
                    {
                        audioSource.PlayOneShot(noGoldSound);
                    }
                    break;
                case "CancelUnitPlacement":

                    UpgradeUnitOnMouse = null;
                    UpgradingStart();
                    break;
            }
        }
    }
    
    public void CancelPlacement()
    {
        PlacingUIText.gameObject.SetActive(false);
        Destroy(placePreviewObject);
        placing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (placing)
        {
            MovePreviewObjectToMouse();
            RotateFromMouseWheel();
        }
        else if (upgrading)
        {
            HighlightUnitOnCursor();
        }
    }

    private void HighlightUnitOnCursor()
    {
        Ray ray = cam.ScreenPointToRay(Mouse.current.position.ReadValue());
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.collider.gameObject.GetComponent<PlaceableUnit>() != null)
            {
                UpgradeUnitOnMouse = hitInfo.collider.gameObject.GetComponent<PlaceableUnit>();
                if(UpgradeUnitOnMouse.upgradeLevel == 3)
                    placementMaterial.SetColor("_BaseColor",Color.red);
                else
                    placementMaterial.SetColor("_BaseColor",Color.green);
            }
            else
            {
                UpgradeUnitOnMouse = null;
            }
        }
        
    }
    
    // Keep gameobject at mouseposition and handle color based if its placeable position
    private void MovePreviewObjectToMouse()
    {
        Ray ray = cam.ScreenPointToRay(Mouse.current.position.ReadValue());

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            //Debug.Log(hitInfo.collider.name);
            if (hitInfo.collider.name != "Terrain")
            {
                objectPlaceable = false;
                placementMaterial.SetColor("_BaseColor",Color.red);
            }
            NavMesh.SamplePosition(hitInfo.point, out NavMeshHit navMeshHit, 0.2f, NavMesh.AllAreas);
            if (!navMeshHit.hit)
            {
                objectPlaceable = false;
                placementMaterial.SetColor("_BaseColor",Color.red);
            }
            else
            {
                objectPlaceable = true;
                placementMaterial.SetColor("_BaseColor",Color.green);
            }
            placePreviewObject.transform.position = hitInfo.point;
            placePreviewObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
    }
    // Rotate unit with mousewheel, does not work with WebGL
    private void RotateFromMouseWheel() 
    {
        placePreviewObject.transform.Rotate(Vector3.up * mouseWheelRotation);
    }
    
    
    
    public void UpgradingStart()
    {
        upgrading = !upgrading;
        UpgradingUIText.gameObject.SetActive(!UpgradingUIText.gameObject.activeSelf);
        foreach (var placedUnit in placedUnits)
        {
            placedUnit.ShowStars(upgrading);
        }
    }
    
    // function to start placing, called by UI
    // id == unitPrefabs index
    public void PlacingStart(int id)
    {
        if(upgrading)
            UpgradingStart();
        if(placing)
            CancelPlacement();
        PlacingUIText.gameObject.SetActive(true);
        var placementPrefab = unitPrefabs[id];
        if (placementPrefab != null)
        {
            placePreviewObject = Instantiate(placementPrefab.gameObject);
            placing = true;
            placePreviewObject.transform.SetLayer(9);
        }
    }

    
}