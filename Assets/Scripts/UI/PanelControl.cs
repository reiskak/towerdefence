﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PanelControl : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerInput inputManager;
    public GameObject buildPanel;
    public GameObject debugPanel;
    private GameObject activePanel;
    private Button[] buttons;
    private void Start()
    {
        inputManager.onActionTriggered += HandleAction;
        SetActivePanel(gameObject);
    }
    
    private void HandleAction(InputAction.CallbackContext context)
    {
        if(context.action.phase != InputActionPhase.Performed)
            return;
        switch (context.action.name)
        {
            case "One":
                ClickButton(0);
                break;
            case "Two":
                ClickButton(1);
                break;
            case "Three":
                ClickButton(2);
                break;
            case "Four":
                ClickButton(3);
                break;
            case "Five":
                ClickButton(4);
                break;
            case "Six":
                ClickButton(5);
                break;
            case "Seven":
                ClickButton(6);
                break;
            case "Eight":
                ClickButton(7);
                break;
            case "Nine":
                ClickButton(8);
                break;
            case "Zero":
                ClickButton(9);
                break;
        }
    }

    private void SetActivePanel(GameObject panel)
    {
        activePanel = panel;
        buttons = activePanel.GetComponentsInChildren<Button>();
    }

    private void ClickButton(int idx)
    {
        if(buttons.Length < idx+1)
            return;
        buttons[idx].onClick.Invoke();
    }

    public void switchPanel(GameObject panel)
    {
        activePanel.SetActive(false);
        panel.SetActive(true);
        SetActivePanel(panel);
    }
    
}
