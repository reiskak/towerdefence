﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateUnitPanel : MonoBehaviour
{
    public GameObject ButtonPrefab;
    public GameObject BackButton;
    public void GenerateButtons(List<PlaceableUnit> units, Action<int> callback)
    {
        // Generate buttons for UnitPlacementPanel
        for (int i = 0; i < units.Count; i++)
        {
            var button = Instantiate(ButtonPrefab, this.transform, false);
            var textFields = button.GetComponentsInChildren<Text>();
            // Hotkey number
            textFields[0].text = (i + 1).ToString();
            // Unit name
            textFields[1].text = units[i].Name;
            // Price
            textFields[2].text = units[i].Price.ToString();
            // Click event to callback with unit index
            int idx = i;
            button.GetComponent<Button>().onClick.AddListener(delegate { callback(idx); });
        }
        // Set back button as last button, and correct number.
        BackButton.transform.SetAsLastSibling();
        BackButton.transform.GetChild(0).GetComponent<Text>().text = (units.Count + 1).ToString();
    }
}
