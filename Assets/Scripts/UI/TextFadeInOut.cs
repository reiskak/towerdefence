﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextFadeInOut : MonoBehaviour
{
    public float ShowFor;
    private Animator animator;
    void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    void OnEnable()
    {
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        animator.SetTrigger("FadeIn");
        yield return new WaitForSeconds(ShowFor);
        animator.SetTrigger("FadeOut");
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }
}
