﻿using System.Collections;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    // Start is called before the first frame update
    public float timer, refresh, avgFramerate;
    string display = "{0}";
    public TMP_Text fpsText;

    // Update is called once per frame
    void Update()
    {
        float timelapse = Time.smoothDeltaTime;
        timer = timer <= 0 ? refresh : timer -= timelapse;
 
        if(timer <= 0) avgFramerate = (int) (1f / timelapse);
        fpsText.text = string.Format(display,avgFramerate.ToString());
    }
}
