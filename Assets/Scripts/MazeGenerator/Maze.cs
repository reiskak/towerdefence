﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Maze : MonoBehaviour
{

    public Vector2Int size;
    [Range(0f, 1f)]
    public float doorProbability;

    public MazeCell cellPrefab;

    private MazeCell[,] cells;
    public float generationStepDelay;
    private byte[,] cellst;

    public MazePassage passagePrefab;
    public MazeWall wallPrefab;

    public Graph<Vector2Int> Graph = new Graph<Vector2Int>();
    private List<MazeRoom> rooms = new List<MazeRoom>();

    public Vector2Int RandomCoordinates
    {
        get
        {
            return new Vector2Int(Random.Range(0,size.x), Random.Range(0,size.y));
        }
    }

    public bool ContainsCoordinates(Vector2Int coordinate)
    {
        return coordinate.x >= 0 && coordinate.x < size.x && coordinate.y >= 0 && coordinate.y < size.y;
    }
    public MazeCell GetCell(Vector2Int coordinates)
    {
        return cells[coordinates.x, coordinates.y];
    }

    private MazeRoom CreateRoom()
    {
        MazeRoom newRoom = ScriptableObject.CreateInstance<MazeRoom>();
        rooms.Add(newRoom);
        return newRoom;
    }

    private void DoFirstGenerationStep(List<MazeCell> activeCells)
    {
        var newCell = CreateCell(RandomCoordinates);
        newCell.Initialize(CreateRoom());
        activeCells.Add(newCell);
    }

    private void CreatePassageInSameRoom(MazeCell cell, MazeCell otherCell, MazeDirection direction)
    {
        MazePassage passage = Instantiate(passagePrefab) as MazePassage;
        passage.Initialize(cell, otherCell, direction);
        Graph.AddEdge(new Tuple<Vector2Int, Vector2Int>(cell.Coordinates, otherCell.Coordinates));
        passage = Instantiate(passagePrefab) as MazePassage;
        passage.Initialize(otherCell, cell, direction.GetOpposite());
    }

    private void CreatePassage(MazeCell cell, MazeCell otherCell, MazeDirection direction)
    {
        MazePassage passage = Instantiate(passagePrefab) as MazePassage;
        passage.Initialize(cell, otherCell, direction);
        Graph.AddEdge(new Tuple<Vector2Int, Vector2Int>(cell.Coordinates,otherCell.Coordinates));
        passage = Instantiate(passagePrefab) as MazePassage;
        otherCell.Initialize(Random.value < doorProbability ? CreateRoom() : cell.room);
        passage.Initialize(otherCell, cell, direction.GetOpposite());
    }

    private void CreateWall(MazeCell cell, MazeCell otherCell, MazeDirection direction)
    {
        MazeWall wall = Instantiate(wallPrefab) as MazeWall;
        wall.Initialize(cell, otherCell, direction);
        if (otherCell != null)
        {
            wall = Instantiate(wallPrefab) as MazeWall;
            wall.Initialize(otherCell, cell, direction.GetOpposite());
        }
    }

    private void DoNextGenerationStep(List<MazeCell> activeCells)
    {
        // int currentIndex = Random.Range(0, activeCells.Count - 1); // Random
        int currentIndex = activeCells.Count - 1; // Last
        //int currentIndex = 0; // First
        //int currentIndex = Mathf.FloorToInt(activeCells.Count / 2); // medium
        MazeCell currentCell = activeCells[currentIndex];
        if (currentCell.IsFullyInitialized)
        {
            activeCells.RemoveAt(currentIndex);
            return;
        }
        MazeDirection direction = currentCell.RandomUninitializedDirection;
        Vector2Int coordinates = currentCell.Coordinates + direction.ToIntVector2();
        if (ContainsCoordinates(coordinates))
        {
            MazeCell neighbor = GetCell(coordinates);
            if (neighbor == null)
            {
                neighbor = CreateCell(coordinates);
                Graph.AddVertex(coordinates);
                CreatePassage(currentCell, neighbor, direction);
                activeCells.Add(neighbor);
            }
            else if (currentCell.room == neighbor.room)
            {
                CreatePassageInSameRoom(currentCell, neighbor, direction);
            }
            else
            {
                CreateWall(currentCell, neighbor, direction);
            }
        }
        else
        {
            CreateWall(currentCell, null, direction);
        }
    }

    public void GenerateNoDelay()
    {
        cells = new MazeCell[size.x, size.y];
        cellst = new byte[size.x, size.y];
        List<MazeCell> activeCells = new List<MazeCell>();
        DoFirstGenerationStep(activeCells);
        while (activeCells.Count > 0)
        {
            DoNextGenerationStep(activeCells);
        }
    }
    public IEnumerator Generate()
    {
        WaitForSeconds delay = new WaitForSeconds(generationStepDelay);
        cells = new MazeCell[size.x, size.y];
        cellst = new byte[size.x,size.y];
        List<MazeCell> activeCells = new List<MazeCell>();
        DoFirstGenerationStep(activeCells);
        while (activeCells.Count > 0)
        {
            yield return delay;
            DoNextGenerationStep(activeCells);
        }
        /*
        for (int x = 0; x < size.x; x++)
        {
            for (int z = 0; z < size.y; z++)
            {
                yield return delay;
                CreateCell(new Vector2Int(x,z));
            }
        }*/
    }

    private MazeCell CreateCell(Vector2Int coordinates)
    {
        MazeCell newCell = Instantiate(cellPrefab) as MazeCell;
        cells[coordinates.x, coordinates.y] = newCell;
        newCell.Coordinates = coordinates;
        newCell.name = "Maze Cell " + coordinates.x + ", " + coordinates.y;
        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.y - size.y * 0.5f + 0.5f);
        return newCell;
    }
}
