﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeWall : MazeCellEdge
{
    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).localRotation = direction.ToRotation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
