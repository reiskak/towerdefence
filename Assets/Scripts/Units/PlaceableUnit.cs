﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PlaceableUnit : MonoBehaviour
{
    public int Price;
    public int UpgradePrice;
    public string Name;
    public GameObject starsGameObject;
    public int upgradeLevel = 1;

    public virtual void Upgrade()
    {
        if (upgradeLevel < 3)
            upgradeLevel++;
        UpdateStars();
    }

    public void UpdateStars()
    {
        starsGameObject.transform.GetChild(upgradeLevel - 1).gameObject.SetActive(true);
    }

    public void ShowStars(bool show)
    {
        starsGameObject.SetActive(show);
    }
    


}
