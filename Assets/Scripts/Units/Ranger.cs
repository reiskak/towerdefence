﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class Ranger : PlaceableUnit
{
    public List<Enemy> enemiesInRange;

    public Enemy currentTarget = null;
    public Animator animator;
    public Arrow arrowPrefab;
    private bool hasTarget = false;
    private Quaternion lookRotation;
    private Vector3 direction;
    public float RotationSpeed = 3f;
    public float AnimationSpeed = 1f;
    public int projectileCount = 2;
    public int damage = 50;

    private bool attackAnimationPlaying = false;
    // Start is called before the first frame update
    void Start()
    {
        enemiesInRange = new List<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.speed = AnimationSpeed;
        if (enemiesInRange.Count > 0)
        {
            // Get new target if no target or target is dead
            if (!hasTarget || currentTarget.health <= 0)
            {
                // Remove target from list if it was dead
                if (hasTarget)
                    enemiesInRange.Remove(currentTarget);
                if(enemiesInRange.Count == 0)
                    return;
                currentTarget = enemiesInRange.First();
                hasTarget = true;
            }
            animator.SetBool("IsAttacking",true);
            //find the vector pointing from our position to the target
            direction = (currentTarget.transform.position - transform.position).normalized;
            //create the rotation we need to be in to look at the target
            lookRotation = Quaternion.LookRotation(direction);
            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * RotationSpeed);
        }
        else
        {
            animator.SetBool("IsAttacking",false);
        }
    }

    public override void Upgrade()
    {
        base.Upgrade();
        projectileCount++;
        damage += 20;
        GetComponent<SphereCollider>().radius *= 1.1f;
    }

    // Function for spawning arrows, called by animation event
    void SpawnArrow()
    {
        var curIdx= enemiesInRange.IndexOf(currentTarget);
        var arrow = Instantiate(arrowPrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        try
        {
            currentTarget.projectilesComing++;
            currentTarget.health -= damage;
            arrow.enemy = currentTarget.transform;
            arrow.Shoot();
        }
        catch
        {
            Destroy(arrow.gameObject);
        }

        // Spawn extra arrows if upgraded
        for (int i = 0; i < projectileCount-1; i++)
        {
            if (enemiesInRange.Count <= curIdx + 1 + i)
            {
                return;
            }
            var extraArrow = Instantiate(arrowPrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
            try
            {
                var extraTarget = enemiesInRange[curIdx + 1 + i];
                extraTarget.projectilesComing++;
                extraTarget.health -= damage;
                extraArrow.enemy = extraTarget.transform;
                extraArrow.Shoot();
            }
            catch
            {
                Destroy(extraArrow.gameObject);
            }
        }
    }
    
    // Handle enemy in range list with trigger
    private void OnTriggerEnter(Collider enemy)
    {
        if (!enemy.name.StartsWith("Enemy"))
            return;
        var e = enemy.gameObject.GetComponent<Enemy>();
        enemiesInRange.Add(e);
    }
    // Handle enemy in range list with trigger
    private void OnTriggerExit(Collider enemy)
    {
        if (currentTarget == null || currentTarget.gameObject == enemy.gameObject)
            hasTarget = false;
        enemiesInRange.Remove(enemy.gameObject.GetComponent<Enemy>());
    }
}
