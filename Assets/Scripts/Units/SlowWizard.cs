﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowWizard : PlaceableUnit
{
    public SlowCircle circle;
    public override void Upgrade()
    {
        base.Upgrade();
        circle.slowAmount *= 1.1f;
        circle.damagePerSecond += 1;
        circle.transform.localScale *= 1.2f;
    }
}
