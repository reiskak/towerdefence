﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SlowCircle : MonoBehaviour
{
    public float slowAmount;
    public int damagePerSecond = 1;
    public List<Enemy> enemiesInRange = new List<Enemy>();
    public float baseSpeed = 3;
    private WaitForSeconds tickDelay;
    private void Start()
    {
        tickDelay = new WaitForSeconds(1f);
        StartCoroutine(DamageOverTime());
    }
    // Coroutine that damages enemies within the debuff circle once a second
    IEnumerator DamageOverTime()
    {
        while (true)
        {
            foreach (var enemy in enemiesInRange)
            {
                enemy.health -= damagePerSecond;
            }
            yield return tickDelay;
        }
    }
    // Slows enemies entering
    private void OnTriggerEnter(Collider enemy)
    {
        if (!enemy.name.StartsWith("Enemy"))
            return;

        var agent = enemy.gameObject.GetComponent<NavMeshAgent>();
        agent.speed *= (1f - slowAmount);
        var e = enemy.gameObject.GetComponent<Enemy>();
        enemiesInRange.Add(e);
    }
    // Restores the speed of leaving enemies
    private void OnTriggerExit(Collider enemy)
    {
        if (!enemy.name.StartsWith("Enemy"))
            return;
        var agent = enemy.gameObject.GetComponent<NavMeshAgent>();
        agent.speed = (agent.speed /(1f - slowAmount));
        enemiesInRange.Remove(enemy.gameObject.GetComponent<Enemy>());
    }
}
