﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    
    public GameObject EnemyPrefab;
    public GameObject EnemyGoal;

    public HashSet<GameObject> Enemies;
    public List<GameObject> Portals;
    public float SpawnDelay;
    
    public bool Spawning = false;
    private int extraHealth;
    
    
    void Start()
    {
        Enemies = new HashSet<GameObject>();
        // Start spawning routine
        StartCoroutine(SpawnRoutine());
        // Subscribe to enemy death event
        Enemy.DeathEvent += EnemyOnDeathEvent;
        GameController.WaveClearEvent += GameControllerOnWaveClearEvent;
    }

    private void GameControllerOnWaveClearEvent(int wave)
    {
        
        extraHealth = (int)Math.Pow((double)wave,(double)2) * 10;
        // 5 waven välein nopeutetaan spawnaysta
        Math.DivRem(wave, 5, out int rem);
        if(rem == 0)
            SpawnDelay *=  0.85f;
    }


    // Debug function, delete all enemies
    public void DeleteEnemies()
    {
        foreach (var enemy in Enemies)
        {
            GameObject.Destroy(enemy);
        }
    }
    // Debug function, speedup all enemies
    public void SpeedUp()
    {
        foreach (var enemy in Enemies)
        {
            enemy.GetComponent<NavMeshAgent>().speed *= 2;
        }
    }
    
    public void ToggleSpawn()
    {
        Spawning = !Spawning;
    }
    
    // Handle enemy cleanup and gold reward
    private void EnemyOnDeathEvent(Enemy enemy)
    {
        GoldManager.instance.Gold += enemy.goldValue;
        Enemies.Remove(enemy.gameObject);
        GameObject.Destroy(enemy.gameObject);
    }

    IEnumerator SpawnRoutine()
    {
        while (true)
        {
            if (Spawning)
            {
                foreach (var portal in Portals)
                {
                    SpawnEnemy(portal);
                }
            }
            yield return new WaitForSeconds(SpawnDelay);
        }
    }
    


    private void SpawnEnemy(GameObject portal)
    {
        var enemy = Instantiate(EnemyPrefab, portal.transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        enemy.GetComponent<Enemy>().health += extraHealth;
        enemy.transform.parent = transform;
        enemy.GetComponent<NavMeshAgent>().SetDestination(EnemyGoal.transform.position);
        enemy.GetComponent<Animator>().SetBool("Moving", true);
        Enemies.Add(enemy);
    }
}
