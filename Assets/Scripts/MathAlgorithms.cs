﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathAlgorithms
{
    // https://www.geeksforgeeks.org/maximum-size-sub-matrix-with-all-1s-in-a-binary-matrix/
    public static void getCenterOfBiggestSquare(byte[,] M,out Vector2 coordinates, out int size) 
    { 
        int i,j; 
        //no of rows in M[,] 
        int R = M.GetLength(0);     
         //no of columns in M[,] 
        int C = M.GetLength(1);     
        int [,]S = new int[R,C];      
          
        int max_of_s, max_i, max_j;  
          
        /* Set first column of S[,]*/
        for(i = 0; i < R; i++) 
            S[i,0] = M[i,0]; 
          
        /* Set first row of S[][]*/
        for(j = 0; j < C; j++) 
            S[0,j] = M[0,j]; 
              
        /* Construct other entries of S[,]*/
        for(i = 1; i < R; i++) 
        { 
            for(j = 1; j < C; j++) 
            { 
                if(M[i,j] == 0)  
                    S[i,j] = Math.Min(S[i,j-1], 
                            Math.Min(S[i-1,j], S[i-1,j-1])) + 1; 
                else
                    S[i,j] = 0; 
            }  
        }      
          
        /* Find the maximum entry, and indexes of  
            maximum entry in S[,] */
        max_of_s = S[0,0]; max_i = 0; max_j = 0; 
        for(i = 0; i < R; i++) 
        { 
            for(j = 0; j < C; j++) 
            { 
                if(max_of_s < S[i,j]) 
                { 
                    max_of_s = S[i,j]; 
                    max_i = i;  
                    max_j = j; 
                }      
            }                  
        }

        var cornerone = new Vector2(max_i, max_j );
        var cornertwo = new Vector2(max_i - max_of_s, max_j - max_of_s);
        coordinates = cornertwo;//(cornerone + cornertwo) / 2;
        size = Math.Abs(max_i - (max_i - max_of_s));
        Debug.Log($"Maximum size sub-matrix is between coordinates: {max_i} {max_j+1} and {max_i-max_of_s} {max_j - max_of_s+1} center: {coordinates} size: {size}"); 
      /*  for(i = max_i; i > max_i - max_of_s; i--) 
        { 
            for(j = max_j; j > max_j - max_of_s; j--) 
            { 
                Debug.Log(M[i,j] + " "); 
            }  
            Debug.Log(""); 
        }  */
    }
}
