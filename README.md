# TowerDefence

## Johdanto
Valitsin lopputyö pelikseni "simppelin" tower defence-pelin, koska siinä voisi tutkia useita itseä kiinnostavia teknisiä asioita. Aloitin työn työstämisen suhteellisen alkukurssista, koska itselläni oli vähän aikaisempaa kokemusta Unity-moottorista ja tein alkupään tehtävät heti alkukurssista. Kuitenkin tämä on ensimmäinen "Proof of Concept" vaihetta pitemmälle mennyt unity projektini.

## Ominaisuudet
### Valmiit ominaisuudet
#### Pelilogiikka
Peli pyörittää kokoajan aalto-looppia, jossa on valmistautumisaika(20s) ja tämän jälkeen vihollisia spawnaa 30s, jonka jälkeen "voitat" kyseisen aallon. Joka aallon jälkeen vihollistet muuttuvat kestävimmiksi ja joka viidennen aallon jälkeen niitten ilmestymisnopeus kasvaa. Lisäksi sinulla on kultaa jota voit käyttää torneihin ja niitten päivittämiseen. Vihollisten tappamisesta saa myös lisää kultaa.

Jos vihollisia karkaa määritelty määrä(50), saat ilmoituksen montako aaltoa selvisit ja mahdollisuuden aloittaa alusta.

#### Viholliset ja spawnerit
EnemyController scripti huolehtii vihollisten spawnaamisesta kentälle asetettuihin portaleihin. Tämän jälkeen viholliset liikkuvat kohti kartan keskellä olevaa "maalia" käyttäen Unityn NavMesh tekniikkaa. Vihollisilla on animaatiot liikkumiseen, vahingon ottamiseen ja kuolemiseen. Logiikka ja animaatorit tehty itse, modellit/animaatiot ladattu.

#### Tornit
Kentälle asetettavat tornit, jotka tekevät vahinkoa vihollisiin ja/tai aiheuttavat erilaisia efektejä. Logiikka ja animaatorit tehty itse, modellit/animaatiot ladattu. Itse tehty simppelit partikkeliefekti wizardin hidastusalueeseen. Toteutin myös pari efektiä Unity VFX Graphilla, mutta nämä eivät näy WebGL buildeissa.

Ranger torni ampuu nuolia kohti vihollisia.

Wizard torni tekee eteensä hidastavaa aluetta, joka hidastaa ja tekee vähän vahinkoa siinä oleviin vihollisiin.

Lisäksi PlacementManager scripti mikä huolehtii näitten tornien ostamis ja kentälle laitto logiikasta.

Mahdollisuus päivittää torneja paremmilla ominaisuuksilla.
* Ranger torni saa enemmän projektiileja ja vahinkoa
* Wizard tornin vahinko, hidastusmäärä ja hidastusalueen koko kasvavat.

#### UI
Toteutettu Unityn omalla UI systeemillä. Koodattu itse muutama scripti UIn dynamiiseen generoitiin.

Infopalkki ylhäällä joka näyttää peliin liittyvää infoa, kuten kullan, ajan ja montako vihollista karannut

Alapalkki josta voi tehdä eri toimintoja kuten ostaa torneja, päivittää torneja... Voi käyttää hiirellä tai 1-9 näppäimillä. Ostopalkkiin tehty scripti, joka tekee uusien tornien lisäämisestä helppoa.

FPS Counter, tähän etsitty suht valmis toteutus.

#### Minimap
Suhteellisen oma toteutus, tehnyt aikaisemmin johonkin omaan projektiin samantyylisen. Ottaa kentästä rendertexturen pelin ladattua ja kamera piirtää toista rendertexturea missä näkyy Minimap layeriin lisätyt objektit tämän texturen päälle.

#### Yksittäisiä pienempiä
* Kamerakontrolleri, jolla voi liikutella kameraa halutuissa rajoissa käyttäen Unityn uutta Input systeemiä.
* Äänet muutamille eri toiminoille
* Post processing


### Kesken jääneet ominaisuudet
#### Terrain generaattori
Aloitin projektin tutustumalla Unityn terrainiin ja miten sitä voisi generoida, koska itsellä oli kova mielenkiinto asiaan. Sain toteutettua jonkunlaisen toteutuksen, jolla lopullisen pelin terrain on generoitu. Kuitenkin tämän ominaisuuden hiomiseen ja "100%" toimivuuteen saaminen veisi aivan liikaa aikaa, joten jätin sen kesken, mutta käytin kuitenkin lopullisen staattisen kentän tekemiseen.

Teknisesti luo määritellyn kokoisen labyrintin, Etsii muutaman reitin labyrintin reunoila keskelle ja luo kartan täyttäen reittien ulkopuoleiset alueet puilla / järvellä. 

## Käytetyt teknologiat
* Unity3D Universal Render Pipeline
* C# Scriptit pääasiassa itse kirjoitettuja.
* Valmiit 3D Modellit

Unityn ominasuudet mm.
* Unity Animator
* Unity New Input System
* Unity VFX & Particle System
* Unity UI
* Unity Terrain

## Optimointi

Yritin ottaa optimoinnin huomioon alusta alkaen.
* Yritin välttää turhia update funtkiota.
* Terrainissa käytin hyödyksi Unityn Terrain komponentin Billboardingia, koska sen avulla pystyin toteuttamaan suhteellisen tiheän metsän suhteellisen pienillä resurssivaatimusilla.
* Meinasin myös käyttää unityn uudemaa DOTS stackkiä ja tein jo pieniä kokeiluja sillä, mutta WebGL ei oikeen varmasti tue näitä, joten jätin tämän toteutuksen siihen.
* Bakettu lightmapit.
* Batching otettu käyttöön ja varmistettu frame debuggerilla toimivan odotestusti.
* Tutkittu myös Unityn Profilerilla pullonkauloja.

Jos pelaa pitkälle, alkaa vihollisten ja tornien määrän noustessa framerate laskea. Testatessa se kuitenkin pysyi testiympäristöissä silti hyvinkin pelattavissa lukemissa.

## Bugit

* Towereitten rotateeminen hiiren rullalla ei toimi WebGL buildissa.
* Jos peliä pelaa pitkälle ja vihollisten ja rangereitten määrä kasvaa isoksi, saattaa esiintyä "jumiin jääneitä nuolia", eli jäävät ilmaan paikalleen.
* Viholliset törmäävät torneihin, joten on mahdollista estää vihollisten eteneminen torneilla.

## Jatkokehitysideoita
 * Pelin balancen parantaminen, tällä hetkellä ei juurikaan mietitty.
 * Enemmän toreja / päivityksiä
 * Terrain generaattorin valmiiksi tekeminen.
 * Enemmän vihollistyyppejä

## Tuntikirjaukset

| Aihealue                                                                     | Pvm       | tunnit |
|------------------------------------------------------------------------------|-----------|--------|
| Terrain generator research and implentation                                  | 17.2.2021 | 3      |
| Terrain generator research and implentation                                  | 18.2.2021 | 3      |
| Terrain generator research and implentation                                  | 19.2.2021 | 3      |
| 3D asset searching, importing and making animation controllers...            | 24.2.2021 | 3      |
| Learning about VFX Graph, making portal effect                               | 24.2.2021 | 1      |
| Unity ECS research, some tests if i could implement it in this project       | 28.2.2021 | 2      |
| Writing some gameplay logic(spawning, enemies, ranger tower...)              | 1.3.2021  | 3      |
| Work on Wizard tower, tower placing logic                                    | 2.3.2021  | 2      |
| Work on UI, switched to new input system                                     | 3.3.2021  | 3      |
| Testing WebGL build, some more UI Work                                       | 12.3.2021 | 3      |
| Added mechanic to actually lose the game                                     | 12.3.2021 | 1      |
| Minimap work, UI text fading, FPS Counter                                    | 23.3.2021 | 4      |
| Tower placing colors, prevent placing outside of navmesh                     | 23.3.2021 | 1      |
| Dynamic unit placement UI, Commenting code                                   | 26.3.2021 | 1      |
| Writing documentation                                                        | 26.3.2021 | 1      |
| Made proper camera controller, Some bug fixes, Added ability to restart game | 29.3.2021 | 2      |
| Controls UI, Upgrading units work                                            | 6.4.2021  | 2      |
| Finished upgrading system, some refactoring, Skybox                          | 9.4.2021  | 3      |
| WebGL compatible particles, added some sounds, writing documentation         | 9.4.2021  | 2      |
| Postproceccing, finalizing, some final bugfixing...                          | 12.4.2021 | 2      |
|                                                                              | total     | 45     |

## Itsearviointi

Opin paljon projektia tehdessä ja käytin siihen mielestäni kurssin opintopisteisiin nähden tarpeeksi aikaa. Lopputyöhön varattu 30-40 tuntia tuli täyteen ja vähän ylikin. Tämä aika on hyvin vähän pelin tekemiseen, varsinkin näin vähällä kokemuksella.

Tästä jos olisi ollut enemmän aikaa olisi seuraavaksi ollut järkevintä balansoida peliä, mutta se ei oikeen tämän kurssin aikataulussa ollut mahdollista, joten peli on tällä hetkellä hyvin helppo, jos vain lisää/päivittää torneja sitä mukaa kun rahaa tulee.

Mielestäni peli onnistui mekaniikoiltaan hyvin, eikä bugeja juuri jäänyt lopulliseen versioon. Tämä oli kuitenkin ensimmäinen Unity projektini, joka ei ole vain yksittäisten asioitten kokeilua ja suunnittelua. Pelissä on paljon omaa koodia ja sitä varten on opeteltu Unitystä paljon uutta, joten uskaltaisin ehdottaa arvosanaksi 4 tai 5.

## Käytetyt resurssit
### 3D
* Vihollis ja "torni" modellit: [https://quaternius.com/]
* Portalin model: [https://sketchfab.com/3d-models/portal-frame-da34b37a224e4e49b307c0b17a50af2c]
* Muut Unity Asset storen ilmaisia assetteja.

### Äänet
* [https://freesound.org/]

### Oppaat
* Terrain generaation pohjalle sovellettuna maze-generaattori: [https://catlikecoding.com/unity/tutorials/maze/]
* VFX Graph: [https://www.youtube.com/watch?v=V2NZb0WN7SY]


